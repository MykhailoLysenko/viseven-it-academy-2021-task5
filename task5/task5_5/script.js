class User {

    constructor(name, surname, email, yearOfBirth) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.yearOfBirth = yearOfBirth;
    }

    getAge() {
        const date = new Date();
        return date.getFullYear() - this.yearOfBirth;
    }

    getFullname() {
        return `${this.name} ${this.surname}`;
    }
}

class Admin extends User {

    read() {
        return `I'm ${this.name}. I can read.`;
    }

    write() {
        return `I'm ${this.name}. I can write.`;
    }

    update() {
        return `I'm ${this.name}. I can update.`;
    }

    remove() {
        return `I'm ${this.name}. I can remove.`;
    }
}

class Moderator extends User {
    read() {
        return `I'm ${this.name}. I can read.`;
    }

    write() {
        return `I'm ${this.name}. I can write.`;
    }

    update() {
        return `I'm ${this.name}. I can update.`;
    }
}

class Client extends User {
    read() {
        return `I'm ${this.name}. I can read.`;
    }

    write() {
        return `I'm ${this.name}. I can write.`;
    }
}

class Guest extends User {
    read() {
        return `I'm ${this.name}. I can read.`;
    }

}

class UserFactory {

    createUser(userPermissions, name, surname, email, yearOfBirth) {

        let userPermissionsByTempate = userPermissions.split(",").map((item) => item.trim()).sort().join("|");

        if (userPermissionsByTempate === 'read|remove|update|write')
            return new Admin(name, surname, email, yearOfBirth);

        if (userPermissionsByTempate === 'read|update|write')
            return new Moderator(name, surname, email, yearOfBirth);

        if (userPermissionsByTempate === 'read|write')
            return new Client(name, surname, email, yearOfBirth);

        if (userPermissionsByTempate === 'read')
            return new Guest(name, surname, email, yearOfBirth);

        throw Error('No suitable permissions specified');

    }
}

const userFactory = new UserFactory();


const user1 = userFactory.createUser("remove, read,  update,write", "Dmytro", "Yummy", "dmytro@test.com", 1995);
const user2 = userFactory.createUser("read, write, update", "Alex", "Morti", "alex@test.com", 1982);
const user3 = userFactory.createUser("read, write", "John", "Smith", "john@test.com", 1976);
const user4 = userFactory.createUser("read", "Robert", "Merlo", "robert@test.com", 1999);


console.log(user1 instanceof Admin);
console.log(user2 instanceof Moderator);
console.log(user3 instanceof Client);
console.log(user4 instanceof Guest);


console.log(user1.getAge());
console.log(user1.getFullname());
console.log(user1.read());
console.log(user1.write());
console.log(user1.update());
console.log(user1.remove());

console.log(user2.getAge());
console.log(user2.getFullname());
console.log(user2.read());
console.log(user2.write());
console.log(user2.update());

console.log(user3.getAge());
console.log(user3.getFullname());
console.log(user3.read());
console.log(user3.write());

console.log(user4.getAge());
console.log(user4.getFullname());
console.log(user4.read());