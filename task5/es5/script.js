export const User = function (name, surname, email, yearOfBirth) {
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.yearOfBirth = yearOfBirth;

    this.getAge = function () {
        const date = new Date();
        return date.getFullYear() - this.yearOfBirth;
    };

    this.getFullname = function () {
        return `${this.name} ${this.surname}`;
    };
};

export const Admin = function (name, surname, email, yearOfBirth) {

    User.call(this, name, surname, email, yearOfBirth);

    Admin.prototype.read = function () {
        return `I'm ${this.name}. I can read.`;
    };

    Admin.prototype.write = function () {
        return `I'm ${this.name}. I can write.`;
    };

    Admin.prototype.update = function () {
        return `I'm ${this.name}. I can update.`;
    };

    Admin.prototype.remove = function () {
        return `I'm ${this.name}. I can remove.`;
    };
};

export const Moderator = function (name, surname, email, yearOfBirth) {

    User.call(this, name, surname, email, yearOfBirth);

    Moderator.prototype.read = function () {
        return `I'm ${this.name}. I can read.`;
    };

    Moderator.prototype.write = function () {
        return `I'm ${this.name}. I can write.`;
    };

    Moderator.prototype.update = function () {
        return `I'm ${this.name}. I can update.`;
    };

};

export const Client = function (name, surname, email, yearOfBirth) {

    User.call(this, name, surname, email, yearOfBirth);

    Client.prototype.read = function () {
        return `I'm ${this.name}. I can read.`;
    };

    Client.prototype.write = function () {
        return `I'm ${this.name}. I can write.`;
    };
};

export const Guest = function (name, surname, email, yearOfBirth) {

    User.call(this, name, surname, email, yearOfBirth);

    Guest.prototype.read = function () {
        return `I'm ${this.name}. I can read.`;
    };
};

const admin = new Admin("Dmytro", "Yummy", "dmytro@test.com", 1995);
const moderator = new Moderator("Alex", "Morti", "alex@test.com", 1982);
const client = new Client("John", "Smith", "john@test.com", 1976);
const guest = new Guest("Robert", "Merlo", "robert@test.com", 1999);

admin.getAge();
admin.getFullname();
admin.read();
admin.write();
admin.update();
admin.remove();

moderator.getAge();
moderator.getFullname();
moderator.read();
moderator.write();
moderator.update();

client.getAge();
client.getFullname();
client.read();
client.write();

guest.getAge();
guest.getFullname();
guest.read();